package faculty;

import java.util.ArrayList;

public class faculty {

    String name; 
    int sizeOffaculty = 0;
    ArrayList<Student> students = new ArrayList<Student>();


    public faculty(String name) { 
        this.name = name; 
       
    }   

        public void addStudent(Student std) {
        students.add(std); 
    } 

        public int getSizeOffaculty() {
                return students.size();
        }
       
  
        public class Student {
            String name;   
            String age;
            

            public Student(String name,  String age) {

                    this.name = name;  
                    this.age = age;
                    
                }    
            } 
        
        public  void main(String[] args) {

            faculty cairo = new faculty("cairo");
            Student mona = new Student("mona","22");
            cairo.addStudent(mona); 

            Student nada = new Student("nada","23");
            cairo.addStudent(nada);

            System.out.println("Number of students in faculty: " + cairo.getSizeOffaculty());
        }
} 
  